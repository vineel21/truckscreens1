import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Navigation2 extends Component {
    render() {
    return (
        <div>
            <NavLink to="/"> </NavLink>
            <NavLink to="/transportfirst"> </NavLink>
            <NavLink to="/transportsecond"></NavLink>
            <NavLink to="/transportthird"></NavLink>
            <NavLink to="/transportfourth"></NavLink>
        </div>
    )
    }
}

export default Navigation2;
