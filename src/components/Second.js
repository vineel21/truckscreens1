import React, { Component } from 'react'
import './mystyle.css'

const initialState = {
    firstName: "",
    lastName: "",
    mobilenumber:"",
    firstNameError:"",
    lastNameError:"",
    mobilenumberError:""
};

class Second extends Component {
    state = initialState;

    handleChange = event => {
        const isCheckbox = event.target.type == "checkbox";
        this.setState({ 
            [event.target.firstName]: isCheckbox 
                ? event.target.checked 
                : event.target.value
        });
        const isCheckbox1 = event.target.type == "checkbox1";
        this.setState({ 
            [event.target.lastName]: isCheckbox1
                ? event.target.checked 
                : event.target.value
        });
    };

    validate = () => {
        let firstNameError = "";
        let lastNameError = "";
        // let mobilenumberError = "";

        if(!this.state.firstName) {
            firstNameError = "firstname cannot be blank";
        }

        if(!this.state.lastName) {
            lastNameError = "lastname cannot be blank";
        }
        
        if(firstNameError || lastNameError) {
            this.setState({ firstNameError, lastNameError });
            return false;
        }
        return true;
    };

    handleSubmit = event => {
        event.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            console.log(this.state);
            this.setState(initialState);
        }
    };


    render() {
    const nav=() => this.props.history.push("/third")
    
    return (
        <form onSubmit={this.handleSubmit}>
            <div className="loginbox" data-component='loginbox'>
            <h1>Register</h1>
            <h2>Already registered?Please</h2>
            <a href='#' > Login</a>
            <div>
                <input type='text' placeholder='First Name' value={this.state.firstname} onChange={this.handleChange} />
                <div style={{ fontSize:15, color:"blue" }}>{this.state.firstNameError}</div>
            </div>
            <div>
                <input type='text' placeholder='Last Name' value={this.state.lastname} onChange={this.handleChange} />
                <div style={{ fontSize:15, color:"blue" }}>{this.state.lastNameError}</div>
            </div>
            <div>
                <input type='text' placeholder='Mobile Number'  />
            </div>
                <input type="submit" name="" value='Register' onClick = {nav}/>
            </div>
        </form>
    )
    }
}

export default Second;

