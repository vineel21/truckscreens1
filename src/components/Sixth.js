import { Map, GoogleApiWrapper } from 'google-maps-react';
import React, { Component } from 'react';

const mapStyles = {
    width: '100%',
    height: '100%',
  }
  
class Sixth extends Component {
    render() {
        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={6}
                    style={mapStyles}
                    initialCenter={{ lat: 20.5937, lng: 78.9629}}
                />
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyDk4ez0J6W0lmziqoiMZ5xjDTcJdkwz5o4'
  })(Sixth);

