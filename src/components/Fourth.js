import React, { Component } from 'react'
import './style2.css'

class Fourth extends Component {
    render() {
        const nav=() => this.props.history.push("/fifth")
    return (
        <div>
            <h1>Welcome Shankar Kondoju!</h1>
        <div className="largebox" data-component='largebox'>
            <h2>Deliver a Package </h2>
            <a href='#' onClick = {nav} > Want to Deliver a Package? </a>    
        </div>
        </div>
    )
    }
}

export default Fourth;
