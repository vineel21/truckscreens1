import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Navigation extends Component {
    render() {
    return (
        <div>
            <NavLink to="/">Image </NavLink>
            <NavLink to="/first"> first </NavLink>
            <NavLink to="/second"></NavLink>
            <NavLink to="/third"></NavLink>
            <NavLink to="/fourth"></NavLink>
            <NavLink to="/fifth"></NavLink>
            <NavLink to="/sixth">Sixth</NavLink>
        </div>
    )
    }
}

export default Navigation;