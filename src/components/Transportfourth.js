import React, { Component } from 'react'
import './mystyles3.css'

class Transportfourth extends Component {
    render() {
        return (
            <div className="Fourthbox" data-component='Fourthbox'>
                <div class="two-col">
                <div class="col1">
                    <label for="field1" >Welcome Company Name!</label>
                <div class="col2">
                    <label for="field2" >Add Trip +</label>
                </div>
                </div>
                </div>
                <div class="two-col">
                <div class="col1">
                    <input id="field1" type="date" id="date" />
                <div class="col2">
                    <input id="field2" type="time" id="time" />
                </div>
                </div>
                </div>
                <div class="two-col">
                <div class="col1">
                    <label for="field1" >Driver Name</label>
                    <input id="field1" type='text' placeholder='Nursing Dobhay'/>
                <div class="col2">
                    <label for="field2" >Mobile Number</label>
                    <input id="field2" type='text' />
                </div>
                </div>
                </div>
                <div class="two-col">
                <div class="col1">
                    <label for="field1" >Delete Trip</label>
                <div class="col2">
                    <input id="field2" type="submit" name="" value='Save Details' />
                </div>
                </div>
                </div>
            </div>
        )
    }
}

export default Transportfourth;

