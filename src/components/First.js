import React, { Component } from 'react'
import Images from './Images.png';
import Images1 from './Images1.png';
import Images2 from './Images2.png';
import './style.css';
import './style3.css';
import './styles4.css';

class First extends Component {
    render() {
        const nav=() => this.props.history.push("/Second")
        const nav1=() => this.props.history.push("/Truckfirst")
        const nav2=() => this.props.history.push("/Transportfirst")
    return (
        <div class="flex-container">
            <img src={Images} className="Images"/>
        <div class="container">
            <button class="btn" onClick = {nav} >Package Owner</button>
        </div>
            <img src={Images1} className="Images1"/>
        <div class="container2">
            <button class="btn2" onClick = {nav1} >Truck Owner</button>
        </div>
            <img src={Images2} className="Images2"/>
        <div class="container3">
            <button class="btn3" onClick = {nav2} >Transport Company</button>
        </div>
        </div>
    )
    }
}

export default First;