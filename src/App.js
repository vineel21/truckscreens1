import './App.css';
import { BrowserRouter,Route,Switch} from 'react-router-dom';
import Image from './components/Image';
import Second from './components/Second';
import First from './components/First';
import Third from './components/Third';
import Fourth from './components/Fourth';
import Fifth from './components/Fifth';
import Sixth from './components/Sixth';
import Truckfirst from './components/Truckfirst';
import Trucksecond from './components/Trucksecond';
import Truckthird from './components/Truckthird';
import Transportfirst from './components/Transportfirst';
import Transportsecond from './components/Transportsecond';
import Transportthird from './components/Transportthird';
import Transportfourth from './components/Transportfourth';
import Navigation from './components/Navigation'
import Navigation1 from './components/Navigation1'
import Navigation2 from './components/Navigation2'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navigation />
        <Navigation1 />
        <Navigation2 />
          <Switch>
            <Route path="/" component={Image} exact />
            <Route path="/first" component={First} exact />
            <Route path="/second" component={Second} exact />
            <Route path="/third" component={Third} exact />
            <Route path="/fourth" component={Fourth} exact />
            <Route path="/fifth" component={Fifth} exact />
            <Route path="/sixth" component={Sixth} exact />
            <Route path="/truckfirst" component={Truckfirst} exact />
            <Route path="/trucksecond" component={Trucksecond} exact />
            <Route path="/truckthird" component={Truckthird} exact />
            <Route path="/transportfirst" component={Transportfirst} exact />
            <Route path="/transportsecond" component={Transportsecond} exact />
            <Route path="/transportthird" component={Transportthird} exact />
            <Route path="/transportfourth" component={Transportfourth} exact />

          </Switch>
        {/* <First />
        <Image />
        <Second /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
